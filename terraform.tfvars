# Tags
project_name        = "TaktTimeCloud"
environment         = "development"
owner               = "TaktTimeCloud"

vpc_security_group_id = "sg-027b8abe5c7e6afbc"
private_subnet_id = "subnet-0849d1d894a1ea8fb"

ec2_instance = {
    name            = "terraform-runner"
    ami             = "ami-08a2aed6e0a6f9c7d"
    instance_type   = "t2.micro"
}