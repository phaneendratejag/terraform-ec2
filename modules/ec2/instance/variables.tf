# variables
variable "name_prefix" {
  description = "Name prefix to be used for all resources"
  type        = string
}

variable "ec2_instance" {
  description = "EC2 instance"
  type = object({
    name          = string
    ami           = string
    instance_type = string
  })
}

variable "security_group_ids" {
  description = "Security group IDs"
  type        = list(string)
}

variable "private_subnet_id" {
  description = "Subnet ID"
  type        = string
}

variable "default_tags" {
  description = "Default tags"
  type        = map(string)
}
