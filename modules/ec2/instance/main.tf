resource "aws_instance" "instance" {
  ami                         = var.ec2_instance.ami
  instance_type               = var.ec2_instance.instance_type
  vpc_security_group_ids      = var.security_group_ids
  subnet_id                   = var.private_subnet_id
  associate_public_ip_address = false

  tags = merge(
    var.default_tags,
    map(
      "Name", "${var.name_prefix}-${var.ec2_instance.name}"
    )
  )
}
