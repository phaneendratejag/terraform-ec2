provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.41"
}

terraform {
  backend "s3" {
    region  = "eu-west-1"
    encrypt = true
    bucket  = "takttime-terraform-states-eu-west-1"
    key     = "dev/terraformrunner.tfstate"
  }
}

# Local variables for the module
locals {
  default_tags = {
    project     = var.project_name
    environment = var.environment
    owner       = var.owner
  }
}


module "ec2_instance" {
  source = "./modules/ec2/instance"

  name_prefix        = "TR-"
  ec2_instance       = var.ec2_instance
  security_group_ids = [var.vpc_security_group_id]
  private_subnet_id  = var.private_subnet_id
  default_tags       = local.default_tags
}
