## ---------------------------------------------------------------- ##
##                              TAGS                                ##
## ---------------------------------------------------------------- ##
variable "project_name" {
  type    = string
  default = "TaktTimeCloud"
}

variable "environment" {
  type    = string
  default = "development"
}

variable "owner" {
  type    = string
  default = "TaktTimeCloud"
}

## ---------------------------------------------------------------- ##
##                              VPC                                 ##
## ---------------------------------------------------------------- ##
variable "vpc_security_group_id" {
  type    = string
  default = "sg-027b8abe5c7e6afbc"
}

variable "private_subnet_id" {
  type    = string
  default = "subnet-0849d1d894a1ea8fb"
}

## ---------------------------------------------------------------- ##
##                            Instances                             ##
## ---------------------------------------------------------------- ##
variable "ec2_instance" {
  description = "EC2 instance"
  type = object({
    name          = string
    ami           = string
    instance_type = string
  })
}
